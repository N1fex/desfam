import React from 'react';
import * as Yup from 'yup';

const validObj = {
  username: Yup.string().required('Обязательное поле').min(3, 'Не менее 3 символов').max(40, 'Не более 40 символов'),
  email: Yup.string().required('Обязательное поле').email('Неверный формат E-mail'),
  password: Yup.string().required('Обязательное поле').min(5, 'Не менее 5 символов'),
  passwordConfirm: Yup.string().required('Обязательное поле').min(5, 'Не менее 5 символов'),
  passwordNotRequired: Yup.string().min(5, 'Не менее 5 символов'),
  datebook: Yup.string().required('Обязательное поле').min(3, 'Не менее 3 символов').max(50, 'Не более 50 символов'),
  position: Yup.string().max(20, 'Не более 20 символов'),
  issue: Yup.string().required('Обязательное поле').min(3, 'Не менее 3 символов'),
  content: Yup.string().required('Обязательное поле').min(3, 'Не менее 3 символов'),
};

export const validateRegistration = ({username, email, password, passwordConfirm} = validObj) => Yup.object({username, email, password, passwordConfirm});
export const validateLogin = ({email, password} = validObj) => Yup.object({email, password});
export const validateAddNewDatebook = ({datebook} = validObj) => Yup.object({datebook});
export const validateAddNewParticipant = ({email} = validObj) => Yup.object({participant: email});
export const validateSettings = ({username, position, passwordNotRequired} = validObj) => Yup.object({username, position, password: passwordNotRequired});
export const validateAddNewIssue = ({issue} = validObj) => Yup.object({issue});
export const validateEditIssue = ({content} = validObj) => Yup.object({content});
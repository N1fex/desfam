import api from './api';

export default {
  async add(invite) {
    return await api().post('invitations', invite);
  },
  async getAll() {
    return await api().get('invitations');
  },
  async accept(id) {
    return await api().get(`invitations/${id}/accept`);
  },
  async reject(id) {
    return await api().delete(`invitations/${id}/reject`);
  },
}
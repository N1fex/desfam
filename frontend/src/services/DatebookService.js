import api from './api';

export default {
  async add(form) {
    return await api().post('datebooks/add', form);
  },
  async getAll() {
    return await api().get('datebooks/getAll');
  },
  async get(id) {
    return await api().get(`datebooks/${id}`);
  },
  async removeParticipant(datebookId, id) {
    return await api().delete(`datebooks/${datebookId}/delete/participant/${id}`);
  },
  async escape(id) {
    return await api().get(`datebooks/${id}/escape`);
  },
  async addIssue(dataReq) {
    return await api().post('issues', dataReq);
  },
  async getIssues(dataReq) {
    return await api().post('issues/get', dataReq);
  },
}
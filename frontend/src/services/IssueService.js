import api from './api';

export default {
  async changeStatus(id) {
    return await api().put(`issues/${id}/status`);
  },
  async delete(id) {
    return await api().delete(`issues/${id}`);
  },
  async edit(form) {
    return await api().put(`issues/${form.id}`, {content: form.content});
  },
}
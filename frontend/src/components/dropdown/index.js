import React from 'react';
import { Dropdown } from 'primereact/dropdown';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';

const MyDropdown = ({value, options, onChange, optionLabel}) => {
  return (
    <Dropdown value={value} options={options} onChange={onChange} optionLabel={optionLabel}/>
  )
};

export default MyDropdown;
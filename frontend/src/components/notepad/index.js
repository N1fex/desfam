import React, {useRef, useState} from 'react';
import * as Styled from './styles';
import {Picshow} from '../picshow';
import Issue from '../issue';
import UIClose from '../ui/ui-close';
import {FieldNotice} from '../../components/field-notice';
import UiBtnSubmit from '../ui/ui-btn-submit';
import {useFormik} from 'formik';
import {validateEditIssue} from '../../utils/validate';
import actions from '../../store/actions';
import {useDispatch} from 'react-redux';

const Notepad = ({notepad}) => {
  const dispatch = useDispatch();

  const [showNotepadSettings, setShowNotepadSettings] = useState(false);
  const [isSubmittingEditIssue, setIsSubmittingEditIssue] = useState(false);
  const [noticeEditIssue, setNoticeEditIssue] = useState({msg: '', type: 'success'});

  const refEditIssue = useRef(null);

  const formikEditIssue = useFormik({
    initialValues: {
      content: '',
      id: ''
    },
    validationSchema: validateEditIssue,
    onSubmit: form => {
      setNoticeEditIssue({msg: '', type: 'success'});
      setIsSubmittingEditIssue(true);

      dispatch(actions.editIssue(form))
        .then(() => {
          setShowNotepadSettings(false)
        })
        .catch(e => setNoticeEditIssue({msg: e.response.data, type: 'error'}))
        .finally(() => setIsSubmittingEditIssue(false))
    },
  });

  const setShowSettings = val => {
    setShowNotepadSettings(val);
    setTimeout(() => refEditIssue.current.focus(), 0);
  };

  const changeIssueContent = e => {
    formikEditIssue.setFieldValue('content', e.target.value)
  };

  return (
    <Styled.NotepadWrap>
      <Styled.NotepadHead>
        <Styled.NotepadAvatar>
          <Picshow pic={`http://localhost:8080${notepad.user.avatar}`}/>
        </Styled.NotepadAvatar>

        <Styled.NotepadUserInfo>
          <Styled.NotepadUsername>{notepad.user.username}</Styled.NotepadUsername>

          <Styled.NotepadUserAddition>{notepad.user.position}</Styled.NotepadUserAddition>
        </Styled.NotepadUserInfo>
      </Styled.NotepadHead>

      <Styled.NotepadPlan>
        <Styled.NotepadList>
          {notepad.issues.map(issue =>
            <Issue issue={issue}
                   setShowNotepadSettings={setShowSettings}
                   formikEditIssue={formikEditIssue}
                   key={issue.id}
            />
          )}
        </Styled.NotepadList>
      </Styled.NotepadPlan>

      {showNotepadSettings && <Styled.NotepadSettings>
        <Styled.NotepadSettingsBlock>
          <Styled.NotepadCloseSettingsBlock>
            <UIClose handler={() => setShowNotepadSettings(false)}/>
          </Styled.NotepadCloseSettingsBlock>

          <Styled.NotepadEditIssueBlock onSubmit={formikEditIssue.handleSubmit}>
            <p>Редактируем задачу</p>

            <div className={'wrap-block'}>
              <div className={'form-group'}>
                <input type={'text'}
                       ref={refEditIssue}
                       className={`${(formikEditIssue.errors.content && formikEditIssue.touched.content) ? 'error' : ''}`}
                       placeholder={'Описание задачи'}
                       {...formikEditIssue.getFieldProps('content')}
                       onChange={changeIssueContent}
                />

                {formikEditIssue.errors.content && formikEditIssue.touched.content && <FieldNotice>{formikEditIssue.errors.content}</FieldNotice>}
              </div>

              <UiBtnSubmit isSubmitting={isSubmittingEditIssue}
                           disabled={!(formikEditIssue.isValid && formikEditIssue.dirty && !isSubmittingEditIssue)}
                           cls={'btn btn-tiny btn-tiny-dark-primary'}
              >
                <i className={'fa fa-pencil'}/>
              </UiBtnSubmit>
            </div>

            {noticeEditIssue.msg && <FieldNotice type={noticeEditIssue.type}>{noticeEditIssue.msg}</FieldNotice>}
          </Styled.NotepadEditIssueBlock>
        </Styled.NotepadSettingsBlock>
      </Styled.NotepadSettings>}
    </Styled.NotepadWrap>
  )
};

export default Notepad;
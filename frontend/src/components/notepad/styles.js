import styled from 'styled-components';

export const NotepadWrap = styled.div`
  background-color: #fff;
  border-radius: 12px;
  padding: 20px;

  @media (max-width: 425px) {
    padding: 15px;
  }
`;

export const NotepadHead = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 10px;
  border-bottom: 1px solid rgba(29, 139, 216, .4);
`;

export const NotepadAvatar = styled.div`
  
`;

export const NotepadUserInfo = styled.div`
  
`;

export const NotepadUsername = styled.div`
  font-size: 20px;
  font-weight: 600;
  text-align: right;

  @media (max-width: 425px) {
    font-size: 18px;
  }
`;

export const NotepadUserAddition = styled.div`
  font-size: 14px;
  text-align: right;

  @media (max-width: 425px) {
    font-size: 12px;
  }
`;

export const NotepadPlan = styled.div`
  padding: 17px 0;
  //height: calc(100% - 76px);

  @media (max-width: 425px) {
    padding: 13px 0;
  }
`;

export const NotepadList = styled.div`
  
`;

export const NotepadSettings = styled.div`
  
`;

export const NotepadSettingsBlock = styled.div`
  position: relative;
  border-top: 1px solid rgba(29, 139, 216, .4);
  padding-top: 15px;
  
  p {
    font-weight: 600;
    font-size: 14px;
  }

  .wrap-block {
    display: flex;
    align-items: flex-start;
    margin-top: 10px;

    button {
      margin-left: 10px;
      line-height: 36px;

      i {
        font-size: 16px;
      }
    }

    .form-group {
      flex: 1;
      input {
        width: 100%;
      }
    }
  }
`;

export const NotepadCloseSettingsBlock = styled.div`
  position: absolute;
  top: 17px;
  right: 0;
`;

export const NotepadEditIssueBlock = styled.form`
  padding: 15px 0;
`;
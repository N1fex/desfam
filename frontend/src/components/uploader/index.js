import React from 'react';
import styled from 'styled-components';

const UploaderWrap = styled.div`
  input {
    opacity: 0;
    visibility: hidden;
    position: absolute;
  }
`;

const Uploader = ({title, id, name, onChange}) => {
  return (
    <UploaderWrap>
      <input id={id} type={'file'} name={name} onChange={onChange} />

      <label htmlFor={id}>
        <span className={'btn btn-primary'}>{title}</span>
      </label>
    </UploaderWrap>
  )
};

export default Uploader;
import React from 'react';
import {Calendar} from 'primereact/calendar';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';

const MyCalendar = ({value, onChange}) => {
  return (
    <Calendar touchUI
              locale={'ru'}
              value={value.toDate()}
              dateFormat={'dd.mm.yy'}
              onChange={onChange}
    />
  )
};

export default MyCalendar;
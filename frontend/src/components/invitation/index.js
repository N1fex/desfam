import React, {useState} from 'react';
import styled from 'styled-components';
import {useDispatch} from 'react-redux';
import actions from '../../store/actions';

export const InvitationWrap = styled.div`
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';
  font-size: 1rem;
  font-weight: normal;
  background: #B3E5FC;
  border: solid #0891cf;
  border-width: 0 0 0 6px;
  color: #044868;
  margin: 1rem 0;
  border-radius: 3px;
  padding: 1rem 1.5rem;
  align-items: center;
  display: flex;
`;

export const InvitationInfo = styled.div`
  
`;

const Invitation = ({invitation}) => {
  const dispatch = useDispatch();

  const [isSubmitting, setIsSubmitting] = useState(false);

  const accept = () => {
    setIsSubmitting(true);

    dispatch(actions.acceptInvitation(invitation.id))
      .finally(() => setIsSubmitting(false))
  };

  const reject = () => {
    setIsSubmitting(true);

    dispatch(actions.rejectInvitation(invitation.id))
      .finally(() => setIsSubmitting(false))
  };

  return (
    <InvitationWrap>
      <div className={'flex flex_1 flex_wrap flex_jc-sb--xs flex_ai-c--xs'}>
        <InvitationInfo>
          {invitation.referrer.username} приглашает в ежедневник
          <b> '{invitation.target.title}'</b>
        </InvitationInfo>

        {isSubmitting ?
          <i className={'fa fa-spinner fa-spin fa-fw'}/> :
          <div className={'flex flex_inline mt-xs-2 mt-md-0 ng-star-inserted'}>
            <button className={'btn btn-success fz-14'} onClick={accept}>Принять</button>

            &nbsp;&nbsp;&nbsp;

            <button className={'btn btn-tiny btn-tiny-danger fz-14'} onClick={reject}>Отклонить</button>
          </div>
        }
      </div>
    </InvitationWrap>
  )
};

export default Invitation;
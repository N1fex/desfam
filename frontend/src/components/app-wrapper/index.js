import styled from 'styled-components';

const AppWrapper = styled.div`
  padding: 0 24px;
  max-width: 1048px;
  width: 100%;
  margin: 0 auto;
  
  @media (max-width: 425px) {
    padding: 0 10px;
  }
`;

export default AppWrapper;
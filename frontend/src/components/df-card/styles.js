import styled from 'styled-components';

export const DfCardWrap = styled.div`
  background: #ffffff;
  border-radius: 3px;
  box-shadow: 0 2px 1px -1px rgb(0 0 0 / 20%), 0 1px 1px 0 rgb(0 0 0 / 14%), 0 1px 3px 0 rgb(0 0 0 / 12%);
  padding: 8px;
`;

export const DfCardHeader = styled.div`
  display: flex;
  align-items: center;
`;

export const DfCardAvatar = styled.div`
  padding-right: 15px;
`;

export const DfCardInfo = styled.div`
  flex: 1;
`;

export const DfCardUsername = styled.div`
  border-bottom: 1px solid #EEF2F8;
  padding: 3px;
  font-weight: 700;
  text-align: right;
`;

export const DfCardHeaderActions = styled.div`
  padding: 5px 0;
  display: flex;
  justify-content: flex-end;
  align-items: center;
`;
import React from 'react';
import * as Styled from './styles';
import {Picshow} from '../picshow';
import avatar from './avatar-plug.jpg';
import {useDispatch} from 'react-redux';
import actions from '../../store/actions';
import {confirm} from '../confirm';

const DfCard = ({usernameParticipant, avatarParticipant, idParticipant, datebookInfo}) => {
  const dispatch = useDispatch();

  const removeParticipant = () => {
    dispatch(actions.removeParticipant(datebookInfo.id, idParticipant))
  };

  return (
    <Styled.DfCardWrap className={'df-card'}>
      <Styled.DfCardHeader>
        <Styled.DfCardAvatar>
          <Picshow pic={avatarParticipant ? `http://localhost:8080${avatarParticipant}` : avatar}/>
        </Styled.DfCardAvatar>

        <Styled.DfCardInfo>
          <Styled.DfCardUsername>{usernameParticipant}</Styled.DfCardUsername>

          <Styled.DfCardHeaderActions>
            <button className={'btn btn-tiny btn-tiny-danger btn-small'} onClick={() => confirm({message: `Убрать ${usernameParticipant} из ежедневника?`, accept: removeParticipant})}> Удалить </button>
          </Styled.DfCardHeaderActions>
        </Styled.DfCardInfo>
      </Styled.DfCardHeader>
    </Styled.DfCardWrap>
  )
};

export default DfCard;
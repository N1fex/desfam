import React from 'react';
import { confirmDialog } from 'primereact/confirmdialog';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';

export const confirm = ({message, accept}) => {
  confirmDialog({
    message, accept,
    icon: 'pi pi-exclamation-triangle',
    acceptLabel: 'Да',
    rejectLabel: 'Нет',
    acceptIcon: 'pi pi-check',
    rejectIcon: 'pi pi-times',
  });
};
import styled from 'styled-components';

export const Component = styled.div`
  position: relative;
  padding: 30px;
  border-radius: 12px;
  background-color: #fff;
  margin-top: 30px;
`;
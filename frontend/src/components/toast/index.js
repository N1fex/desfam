import React from 'react';
import {Toast} from 'primereact/toast';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

export const refToast = React.createRef();

const MyToast = () => <Toast ref={refToast} className={'alert'}/>;

export default MyToast;
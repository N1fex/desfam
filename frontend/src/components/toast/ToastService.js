import {refToast} from './index';

export const ToastService = {
  show: (detail, severity = 'success', life = 3000, closable = false) => refToast.current.show({detail, severity, life, closable})
};
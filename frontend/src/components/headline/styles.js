import styled from 'styled-components';

export const HeadlineWrap = styled.div`
  font-weight: 300;
  font-size: 20px;
  margin: 20px 0;
  
  @media (max-width: 425px) {
    font-size: 18px;
  }
`;

export const HeadlineDate = styled.div`
  margin: 15px;
  display: flex;
  justify-content: space-between;
  
  @media (max-width: 425px) {
    margin: 10px;
  }
`;

export const HeadlineDay = styled.div`
  
`;

export const HeadlineFullDate = styled.div`
  font-weight: 400;
  position: relative;
`;

export const HeadlineHr = styled.div`
  height: 1px;
  background: linear-gradient(to right, rgba(202, 214, 244, 0.15) 0%, #cad6f4 50%, rgba(202, 214, 244, 0.15) 100%);
`;
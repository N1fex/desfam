import React from 'react';
import * as Styled from './styles';
import MyCalendar from '../calendar';
import {titleCase} from '../../utils/titleCase';

const Headline = ({date, onChange}) => {
  return (
    <Styled.HeadlineWrap>
      <Styled.HeadlineDate>
        <Styled.HeadlineDay>{titleCase(date.format('dddd'))}</Styled.HeadlineDay>

        <Styled.HeadlineFullDate>
          <MyCalendar value={date} onChange={onChange} />
        </Styled.HeadlineFullDate>
      </Styled.HeadlineDate>

      <Styled.HeadlineHr/>
    </Styled.HeadlineWrap>
  )
};

export default Headline;
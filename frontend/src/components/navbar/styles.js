import styled from 'styled-components';

export const NavbarWrap = styled.div`
  padding: 20px 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const NavbarLogo = styled.img`
  width: 70px;
  
  @media (max-width: 425px) {
    width: 45px;
  }
`;

export const NavbarPanel = styled.div`
  display: flex;
  align-items: center;
  
  & > * {
    margin: 0 10px;
    
    &:last-child {
      margin-right: 0;
    }
  }
  
  a {
    font-weight: 300;
  }
  
  i {
    font-size: 20px;
  }
  
  .navbar__settings {
    color: #828EA5;
    
    &:hover {
      color: #5b687f;
    }
  }
  
  .navbar__logout {
    color: #EE246D;
    
    &:hover {
      color: #b70e4b;
    }
  }
`;
import React from 'react';
import {Link} from 'react-router-dom';
import '../../assets/styles/font-awesome-4.7.0/css/font-awesome.min.css';
import * as Styled from './styles';
import {Picshow} from '../picshow';
import avatar from './avatar-plug.jpg'
import {useHistory} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import actions from '../../store/actions';

const Navbar = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const currentUser = useSelector(state => state.auth.currentUser);

  return (
    <Styled.NavbarWrap>
      <Link to={'/'}>
        <Styled.NavbarLogo src={'/img/elements/logo.svg'} />
      </Link>

      <Styled.NavbarPanel>
        {currentUser && <>
          <Link to={'/settings'} className={'link link-tiny navbar__settings'}>
            <i className={'fa fa-cog'}/>
          </Link>

          <Picshow pic={currentUser.avatar ? `http://localhost:8080${currentUser.avatar}` : avatar}/>

          <button
            className={'btn btn-tiny navbar__logout'}
            onClick={() => dispatch(actions.logout())
              .then(() => history.push('/login'))
            }
          >
            <i className={'fa fa-sign-out'}/>
          </button>
        </>}

        {!currentUser && <>
          <Link to={'/registration'} className={'link link-tiny'}>Регистрация</Link>

          <Link to={'/login'} className={'link link-tiny'}>Вход</Link>
        </>}
      </Styled.NavbarPanel>
    </Styled.NavbarWrap>
  );
};

export default Navbar;
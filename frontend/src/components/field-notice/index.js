import styled, {css} from 'styled-components';

export const FieldNotice = styled.div`
  font-size: 12px;
  margin: 8px 0;
  color: #EE246D;
  
  ${props => (props.type === 'success' && css`
    color: #6AD281;
  `)}
  
  ${props => (props.type === 'error' && css`
    color: #EE246D;
  `)}
  
  ${props => (props.type === 'ordinary' && css`
    color: #ced1db;
  `)}
`;
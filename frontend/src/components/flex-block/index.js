import styled from 'styled-components';

const FlexBlock = styled.div`
  display: flex;
  flex-wrap: ${props => props.wrap || 'nowrap'};
  flex-direction: ${props => props.direction || 'row'};
  flex: ${props => props.flex || 'auto'};
  align-items: ${props => props.align || 'stretch'};
`;

export default FlexBlock;
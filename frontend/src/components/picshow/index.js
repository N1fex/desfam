import React, {useCallback, useEffect, useRef} from 'react';
import * as Styled from './styles';
import LightGallery from 'lightgallery/react';
import 'lightgallery/scss/lightgallery.scss';

export const Picshow = ({pic}) => {
  const lightGallery = useRef(null);

  useEffect(() => {
    lightGallery.current.refresh();
  }, [pic]);

  const onInit = useCallback((detail) => {
    if (detail) {
      lightGallery.current = detail.instance;
    }
  }, []);

  const settings = {
    counter: false,
    download: false,
    enableDrag: false,
    backdropDuration: 150
  };

  return (
    <Styled.PicshowWrap>
      <LightGallery {...settings} onInit={onInit}>
        <a href={pic}><img src={pic} alt={''}/></a>
      </LightGallery>
    </Styled.PicshowWrap>
  )
};
import styled from 'styled-components';

export const PicshowWrap = styled.div`
  line-height: 0;
  
  img {
    width: 60px;
    height: 60px;
    object-fit: cover;
    border-radius: 50%;
    
    @media (max-width: 425px) {
      width: 45px;
      height: 45px;
    }
  }
`;
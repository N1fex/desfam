import styled from 'styled-components';

export const ListDatebooksWrap = styled.div`
  h2 {
    margin-bottom: 20px;
  }
`;

export const ListDatebooksItem = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 5px;
  
  &:last-child {
    margin-bottom: 0;
  }
`;

export const ListDatebooksItemIcon = styled.div`
  margin-right: 15px;
  display: flex;
  
  i {
    font-size: 13px;
  }
  
  &-standard {
    width: 8px;
    height: 8px;
    background-color: #1D8BD8;
    border-radius: 50%;
  }
`;

export const ListDatebooksItemName = styled.div`
  color: #29aaac;
`;
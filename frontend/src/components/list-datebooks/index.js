import React from 'react';
import {Link} from 'react-router-dom';
import * as Styled from './styles';
import {useSelector} from 'react-redux';

const ListDatebooks = ({datebooks}) => {
  const currentUser = useSelector(state => state.auth.currentUser);

  return (
    <Styled.ListDatebooksWrap>
      <h2>Все задачники</h2>

      {datebooks.map(datebook => <Styled.ListDatebooksItem key={datebook.id}>
        <Styled.ListDatebooksItemIcon>
          {currentUser.id === datebook.creator && <i className={'fa fa-user-circle-o fa-fw icon-primary'}/>}
          {currentUser.id !== datebook.creator && <i className={'fa fa-circle fa-fw icon-primary'}/>}
        </Styled.ListDatebooksItemIcon>

        <Styled.ListDatebooksItemName>
          <Link to={`/datebooks/${datebook.id}`}
                className={'link link-tiny link-green'}>{datebook.title}
          </Link>
        </Styled.ListDatebooksItemName>
      </Styled.ListDatebooksItem>)}
    </Styled.ListDatebooksWrap>
  )
};

export default ListDatebooks;
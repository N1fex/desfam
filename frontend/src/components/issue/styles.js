import styled, {css} from 'styled-components';

export const IssueWrap = styled.div`
  margin: 10px 0;
  position: relative;
  cursor: pointer;
`;

export const IssueText = styled.div`
  background-color: #EEF2F8;
  padding: 3px 30px 3px 10px;
  border-radius: 5px;
  font-size: 14px;
  transition: all .3s;
  
  &:hover { 
    background-color: #d5dfee; 
  }

  ${props => props.active && css`
    background-color: #d5dfee;
  `}
  
  ${props => props.ready && css`
    background-color: #99e0a9;
    
    &:hover {
      background-color: #7ad78e;
    }
  `}
`;

export const IssueCreator = styled.div`
  position: absolute;
  top: -7px;
  left: -5px;

  & img {
    width: 17px!important;
    height: 17px!important;
  }
`;

export const IssueSettings = styled.div`
  position: absolute;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 2;
  background-color: rgb(201, 234, 242);
  padding: 10px;
  border-radius: 5px;
`;
import React, {useEffect, useRef, useState} from 'react';
import * as Styled from './styles';
import {Picshow} from '../picshow';
import {Button} from '../button';
import {Icon} from '../icon';
import {useDispatch, useSelector} from 'react-redux';
import actions from '../../store/actions/issue';
import moment from 'moment';

const Issue = ({issue, setShowNotepadSettings, formikEditIssue}) => {
  const dispatch = useDispatch();

  const currentUser = useSelector(state => state.auth.currentUser);

  const [showSettings, setShowSettings] = useState(false);

  const refIssue = useRef(null);

  const setIssueContent = () => {
    formikEditIssue.setFieldValue('content', issue.content);
    formikEditIssue.setFieldValue('id', issue.id);
    setShowSettings(false);
    setShowNotepadSettings(true);
  };

  const getParticipantRightToOpenSettings = () => {
    if (currentUser.id === issue.creator.id || currentUser.id === issue.target.id) setShowSettings(!showSettings)
  };

  const changeStatusIssue = () => {
    dispatch(actions.changeStatusIssue(issue.id))
      .then(() => setShowSettings(!showSettings))
  };

  const deleteIssue = () => {
    dispatch(actions.deleteIssue(issue.id))
  };

  const handler = e => {
    if (e.target.closest('.issue') !== refIssue.current) setShowSettings(false)
  };

  useEffect(() => {
    document.addEventListener('click', handler);
    return () => document.removeEventListener('click', handler);
  }, []);

  return (
    <Styled.IssueWrap className={'issue'} ref={refIssue}>
      <Styled.IssueText active={showSettings}
                        ready={issue.status}
                        onClick={getParticipantRightToOpenSettings}
      >
        {issue.content}
      </Styled.IssueText>

      {issue.target.id !== issue.creator.id &&
        <Styled.IssueCreator>
          <Picshow pic={`http://localhost:8080${issue.creator.avatar}`}/>
        </Styled.IssueCreator>
      }

      {showSettings && moment(issue.date).isSameOrAfter(moment(), 'd') &&
        <Styled.IssueSettings>
          {issue.status &&
            <Button className={'btn btn-tiny'} set cancel onClick={changeStatusIssue}>
              <Icon className={'fa fa-undo fa-fw'} /> Не готово
            </Button>
          }

          {!issue.status &&
            <Button className={'btn btn-tiny'} set ready onClick={changeStatusIssue}>
              <Icon className={'fa fa-check fa-fw'} /> Готово
            </Button>
          }

          {currentUser.id === issue.creator.id && <>
            <Button className={'btn btn-tiny'} set edit onClick={setIssueContent}>
              <Icon className={'fa fa-edit fa-fw'} /> Редактировать
            </Button>

            <Button className={'btn btn-tiny'} set delete onClick={deleteIssue}>
              <Icon className={'fa fa-remove fa-fw'} /> Удалить
            </Button>
          </>}
        </Styled.IssueSettings>
      }
    </Styled.IssueWrap>
  )
};

export default Issue;
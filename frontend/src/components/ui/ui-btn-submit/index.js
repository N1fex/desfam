import React from 'react';

const UiBtnSubmit = ({children, isSubmitting, disabled, cls}) => {
  return (
    <button type={'submit'} disabled={disabled} className={cls}>
      {isSubmitting ? <i className={'fa fa-spinner fa-spin fa-fw'}/> : children}
    </button>
  )
};

export default UiBtnSubmit;
import React from 'react';
import styled, {css} from 'styled-components';
import {Icon} from '../icon';

export const Button = styled.button`
  ${props => props.set && css`
    font-size: 14px!important;
    line-height: 26px;
    cursor: pointer!important;
    display: block;
    width: 100%;
    
    ${Icon} {
      transition: all .1s;
    }
  `}
  
  ${props => props.ready && css`
    &:hover ${Icon} {
      color: #6AD281;
    }
  `}
  
  ${props => props.edit && css`
    &:hover ${Icon} {
      color: #C12497;
    }
  `}
  
  ${props => props.delete && css`
    &:hover ${Icon} {
      color: #EE246D;
    }
  `}
  
  ${props => props.cancel && css`
    &:hover ${Icon} {
      color: #828EA5;
    }
  `}
`;
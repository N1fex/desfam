import React, {useState} from 'react';
import MyDropdown from '../dropdown';
import {useFormik} from 'formik';
import {validateAddNewIssue} from '../../utils/validate';
import {FieldNotice} from '../field-notice';
import actions from '../../store/actions';
import {useDispatch} from 'react-redux';
import UiBtnSubmit from '../ui/ui-btn-submit';

const FormAddNewIssue = ({currentUser, datebook, date}) => {
  const dispatch = useDispatch();

  const [isSubmittingAddNewIssue, setIsSubmittingAddNewIssue] = useState(false);
  const [noticeAddNewIssue, setNoticeAddNewIssue] = useState({msg: '', type: 'success'});

  const [executor, setExecutor] = useState(datebook.participants.find(el => el.id === currentUser.id));

  const changeExecutor = e => setExecutor(e.value);

  const formikAddNewIssue = useFormik({
    initialValues: {
      issue: ''
    },
    validationSchema: validateAddNewIssue,
    onSubmit: form => {
      setNoticeAddNewIssue({msg: '', type: 'success'});
      setIsSubmittingAddNewIssue(true);

      dispatch(actions.addIssue({date, datebook: datebook.id, content: form.issue, target: executor.id}))
        .then(() => {
          formikAddNewIssue.resetForm();
        })
        .catch(e => setNoticeAddNewIssue({msg: e.response.data, type: 'error'}))
        .finally(() => setIsSubmittingAddNewIssue(false))
    },
  });

  return (
    <form className={'form form_add-new-issue'} onSubmit={formikAddNewIssue.handleSubmit}>
      <div className={'form__group'}>
        <h4>Добавить новую задачу</h4>

        <div className={'form__horizontal form__vertical-start form__nowrap'}>
          <div className={'flex flex__1 flex__column'}>
            <input type={'text'}
                   className={`${(formikAddNewIssue.errors.issue && formikAddNewIssue.touched.issue) ? 'error' : ''}`}
                   placeholder={'Опиши задачу'}
                   {...formikAddNewIssue.getFieldProps('issue')}
            />

            {formikAddNewIssue.errors.issue && formikAddNewIssue.touched.issue && <FieldNotice>{formikAddNewIssue.errors.issue}</FieldNotice>}
          </div>

          <UiBtnSubmit isSubmitting={isSubmittingAddNewIssue}
                       disabled={!(formikAddNewIssue.isValid && formikAddNewIssue.dirty && !isSubmittingAddNewIssue)}
                       cls={'btn btn-submit btn-tiny btn-tiny-dark-primary'}
          >
            <i className={'fa fa-plus'}/>
          </UiBtnSubmit>
        </div>

        <div className={'executor'}>
          <span>Назначить на:</span>

          <MyDropdown value={executor}
                      options={datebook.participants}
                      optionLabel={'username'}
                      onChange={changeExecutor}
          />
        </div>
      </div>
    </form>
  )
};

export default FormAddNewIssue;
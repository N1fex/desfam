import IssueService from '../../services/IssueService';

const issueActions = {
  changeStatusIssueSuccess: (payload) => ({type: 'CHANGE_STATUS_ISSUE_SUCCESS', payload}),
  changeStatusIssue: (id) => async (dispatch) => {
    return IssueService.changeStatus(id)
      .then(res => {
        dispatch(issueActions.changeStatusIssueSuccess(res.data));
      })
  },

  deleteIssueSuccess: (payload) => ({type: 'DELETE_ISSUE_SUCCESS', payload}),
  deleteIssue: (id) => async (dispatch) => {
    return IssueService.delete(id)
      .then(() => {
        dispatch(issueActions.deleteIssueSuccess({id}));
      })
  },

  editIssueSuccess: (payload) => ({type: 'EDIT_ISSUE_SUCCESS', payload}),
  editIssue: (form) => async (dispatch) => {
    return IssueService.edit(form)
      .then(res => {
        dispatch(issueActions.editIssueSuccess(res.data));
      })
  },
};

export default issueActions;
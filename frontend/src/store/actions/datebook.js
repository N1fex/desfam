import DatebookService from '../../services/DatebookService';
import {ToastService} from '../../components/toast/ToastService';

const datebookActions = {
  addDatebookSuccess: (payload) => ({type: 'ADD_DATEBOOK_SUCCESS', payload}),
  addDatebook: (form) => async (dispatch) => {
    return DatebookService.add(form)
      .then(res => {
        dispatch(datebookActions.addDatebookSuccess(res.data));
      })
  },

  getAllDatebooksSuccess: (payload) => ({type: 'GET_ALL_DATEBOOKS_SUCCESS', payload}),
  getAllDatebooks: () => async (dispatch) => {
    return DatebookService.getAll()
      .then(res => {
        dispatch(datebookActions.getAllDatebooksSuccess(res.data));
      })
  },

  getDatebookSuccess: (payload) => ({type: 'GET_DATEBOOK_SUCCESS', payload}),
  getDatebook: (id) => async (dispatch) => {
    return DatebookService.get(id)
      .then(res => {
        dispatch(datebookActions.getDatebookSuccess(res.data));
      })
  },

  removeParticipantSuccess: (payload) => ({type: 'REMOVE_PARTICIPANT_SUCCESS', payload}),
  removeParticipant: (datebookId, id) => async (dispatch) => {
    return DatebookService.removeParticipant(datebookId, id)
      .then(res => {
        dispatch(datebookActions.removeParticipantSuccess({datebookId, id}));
        ToastService.show(res.data)
      })
      .catch(e => ToastService.show(e.response.data, 'error'))
  },

  escapeDatebookSuccess: (payload) => ({type: 'ESCAPE_DATEBOOK_SUCCESS', payload}),
  escapeDatebook: (id) => async (dispatch) => {
    return DatebookService.escape(id)
      .then(res => {
        dispatch(datebookActions.escapeDatebookSuccess(res.data));
        ToastService.show(res.data);
      })
      .catch(e => ToastService.show(e.response.data, 'error'))
  },

  clearDatebookSuccess: () => ({type: 'CLEAR_DATEBOOK_SUCCESS'}),
  clearDatebook: () => async (dispatch) => {
    dispatch(datebookActions.clearDatebookSuccess())
  },

  addIssueSuccess: (payload) => ({type: 'ADD_ISSUE_SUCCESS', payload}),
  addIssue: (dataReq) => async (dispatch) => {
    return DatebookService.addIssue(dataReq)
      .then(res => {
        dispatch(datebookActions.addIssueSuccess(res.data));
      })
  },

  getIssuesSuccess: (payload) => ({type: 'GET_ISSUES_SUCCESS', payload}),
  getIssues: (dataReq) => async (dispatch) => {
    return DatebookService.getIssues(dataReq)
      .then(res => {
        dispatch(datebookActions.getIssuesSuccess(res.data));
      })
  },
};

export default datebookActions;
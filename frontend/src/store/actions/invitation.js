import InvitationService from '../../services/InvitationService';
import {ToastService} from '../../components/toast/ToastService';

const invitationActions = {
  getAllInvitationsSuccess: (payload) => ({type: 'GET_ALL_INVITATIONS_SUCCESS', payload}),
  getAllInvitations: () => async (dispatch) => {
    return InvitationService.getAll()
      .then(res => {
        dispatch(invitationActions.getAllInvitationsSuccess(res.data));
      })
  },

  acceptInvitationSuccess: (payload) => ({type: 'ACCEPT_INVITATION_SUCCESS', payload}),
  acceptInvitation: (id) => async (dispatch) => {
    return InvitationService.accept(id)
      .then(res => {
        dispatch(invitationActions.acceptInvitationSuccess(res.data));
        ToastService.show(`${res.data}`);
      })
      .catch(e => ToastService.show(e.response.data, 'error'))
  },

  rejectInvitationSuccess: (payload) => ({type: 'REJECT_INVITATION_SUCCESS', payload}),
  rejectInvitation: (id) => async (dispatch) => {
    return InvitationService.reject(id)
      .then(() => {
        dispatch(invitationActions.rejectInvitationSuccess({id}));
      })
  },
};

export default invitationActions;
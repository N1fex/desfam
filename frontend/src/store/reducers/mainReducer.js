import {ADD_DATEBOOK_SUCCESS, GET_ALL_DATEBOOKS_SUCCESS, GET_ALL_INVITATIONS_SUCCESS,
  LOGOUT_SUCCESS, ACCEPT_INVITATION_SUCCESS, REJECT_INVITATION_SUCCESS} from '../constants';

const defaultState = {
  datebookList: [],
  invitations: []
};

export const mainReducer = (state = defaultState, action) => {
  const {type, payload} = action;
  switch (type) {
    case ADD_DATEBOOK_SUCCESS:
      return {
        ...state,
        datebookList: [...state.datebookList, payload]
      };

    case GET_ALL_DATEBOOKS_SUCCESS:
      return {
        ...state,
        datebookList: payload
      };

    case GET_ALL_INVITATIONS_SUCCESS:
      return {
        ...state,
        invitations: [...payload]
      };

    case LOGOUT_SUCCESS:
      return {
        ...state,
        datebookList: [],
        invitations: []
      };

    case ACCEPT_INVITATION_SUCCESS:
      return {
        ...state,
        datebookList: [...state.datebookList, payload],
        invitations: [...state.invitations].filter(el => el.target.id != payload.id)
      };

    case REJECT_INVITATION_SUCCESS:
      return {
        ...state,
        invitations: [...state.invitations].filter(el => el.id !== payload.id)
      };

    default:
      return state
  }
};
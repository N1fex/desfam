import {GET_DATEBOOK_SUCCESS, REMOVE_PARTICIPANT_SUCCESS,
        ESCAPE_DATEBOOK_SUCCESS, CLEAR_DATEBOOK_SUCCESS,
        ADD_ISSUE_SUCCESS, GET_ISSUES_SUCCESS,
        CHANGE_STATUS_ISSUE_SUCCESS, DELETE_ISSUE_SUCCESS,
        EDIT_ISSUE_SUCCESS} from '../constants';

const defaultState = {
  info: null,
  issues: null
};

export const datebookReducer = (state = defaultState, action) => {
  const {type, payload} = action;
  switch (type) {
    case GET_DATEBOOK_SUCCESS:
      return {
        ...state,
        info: payload
      };

    case REMOVE_PARTICIPANT_SUCCESS:
      return {
        ...state,
        info: {...state.info, participants: [...state.info.participants].filter((el => el.id !== payload.id))}
      };

    case ESCAPE_DATEBOOK_SUCCESS:
      return {
        ...state,
        info: {...state.info, participants: [...state.info.participants].filter((el => el.id !== payload.id))}
      };

    case CLEAR_DATEBOOK_SUCCESS:
      return {
        ...state,
        info: null,
        issues: null
      };

    case ADD_ISSUE_SUCCESS:
      return {
        ...state,
        issues: [...state.issues, payload]
      };

    case GET_ISSUES_SUCCESS:
      return {
        ...state,
        issues: payload
      };

    case CHANGE_STATUS_ISSUE_SUCCESS:
      return {
        ...state,
        issues: [...state.issues].map(el => {
          let issue = {...el};
          if (issue.id === payload.id) issue.status = payload.status;

          return issue;
        })
      };

    case DELETE_ISSUE_SUCCESS:
      return {
        ...state,
        issues: [...state.issues].filter(el => el.id !== payload.id)
      };

    case EDIT_ISSUE_SUCCESS:
      return {
        ...state,
        issues: [...state.issues].map(el => {
          let issue = {...el};
          if (issue.id === payload.id) issue.content = payload.content;

          return issue;
        })
      };

    default:
      return state
  }
};
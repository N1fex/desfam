import React, {useEffect, useState} from 'react';
import {Component} from '../../components/component';
import {FieldNotice} from '../../components/field-notice';
import {Picshow} from '../../components/picshow';
import Uploader from '../../components/uploader';
import avatar from './avatar-plug.jpg';
import {useFormik} from 'formik';
import {validateSettings} from '../../utils/validate';
import {useDispatch, useSelector} from 'react-redux';
import UiBtnSubmit from '../../components/ui/ui-btn-submit';
import actions from '../../store/actions';

const SettingsModule = () => {
  const dispatch = useDispatch();

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [notice, setNotice] = useState({msg: '', type: 'success'});
  const [tempAvatar, setTempAvatar] = useState('');

  const currentUser = useSelector(state => state.auth.currentUser);

  const formik = useFormik({
    initialValues: {
      username: '',
      position: '',
      avatar: '',
      password: ''
    },
    validationSchema: validateSettings,
    onSubmit: form => {
      setNotice({msg: '', type: 'success'});
      setIsSubmitting(true);

      const formData = new FormData();
      Object.keys(form).forEach(key => {
        formData.append(key, form[key])
      });

      dispatch(actions.updateCurrentUser(formData))
        .then(() => setNotice({msg: 'Изменения сохранены', type: 'success'}))
        .catch(e => setNotice({msg: e.response.data, type: 'error'}))
        .finally(() => setIsSubmitting(false))
    },
  });

  const getSettingsAvatar = () => {
    if (tempAvatar) return tempAvatar;

    if (currentUser.avatar) {
      let prefix = process.env.NODE_ENV === 'development' ? 'http://localhost:8080' : '';
      return `${prefix}${currentUser.avatar}`;
    }
    else return avatar;
  };

  useEffect(() => {
    if (currentUser) {
      Object.keys(formik.initialValues).forEach(field => {
        if (currentUser.hasOwnProperty(field)) formik.setFieldValue(field, currentUser[field]);
      })
    }
  }, [currentUser]);

  return (
    <Component>
      {currentUser && <>
        <h1>Настройки пользователя</h1>

        <form className={'form'} onSubmit={formik.handleSubmit}>
          <h2>Информация</h2>

          <div className={'form__group'}>
            <label>Назови себя</label>

            <input type={'text'}
                   className={`${(formik.errors.username && formik.touched.username) ? 'error' : ''}`}
                   placeholder={'Желательно ФИО'}
                   {...formik.getFieldProps('username')}
            />

            {(formik.errors.username && formik.touched.username) && <FieldNotice>{formik.errors.username}</FieldNotice>}
          </div>

          <div className={'form__group'}>
            <label>Кто по жизни?</label>

            <input type={'text'}
                   className={`${(formik.errors.position && formik.touched.position) ? 'error' : ''}`}
                   placeholder={'Например: Менеджер'}
                   {...formik.getFieldProps('position')}
            />

            {(formik.errors.position && formik.touched.position) && <FieldNotice>{formik.errors.position}</FieldNotice>}
          </div>

          <div className={'form__group'}>
            <label>Аватарка</label>

            <div className={'form__horizontal form__vertical-center'}>
              <Picshow pic={getSettingsAvatar()}/>

              &nbsp;&nbsp;&nbsp;

              <Uploader id={'uploader_avatar'}
                        name={'avatar'}
                        title={'Новый аватар'}
                        onChange={e => {
                          formik.setFieldValue('avatar', e.target.files[0]);
                          setTempAvatar(URL.createObjectURL(e.target.files[0]))
                        }}
              />
            </div>
          </div>

          <br/>

          <h2>Сменить пароль</h2>

          <div className={'form__group'}>
            <label>Новый пароль</label>

            <input type={'password'}
                   className={`${(formik.errors.password && formik.touched.password) ? 'error' : ''}`}
                   {...formik.getFieldProps('password')}
            />

            {(formik.errors.password && formik.touched.password) && <FieldNotice>{formik.errors.password}</FieldNotice>}
          </div>

          {notice.msg && <FieldNotice type={notice.type}>{notice.msg}</FieldNotice>}

          <div className={'form__actions'}>
            <UiBtnSubmit isSubmitting={isSubmitting}
                         disabled={!(formik.isValid && formik.dirty && !isSubmitting)}
                         cls={'btn btn-primary'}
            >
              Сохранить
            </UiBtnSubmit>
          </div>
        </form>
      </>}
    </Component>
  )
};

export default SettingsModule;
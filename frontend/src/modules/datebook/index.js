import React, {useCallback, useEffect, useState} from 'react';
import * as Styled from './styles';
import {useDispatch, useSelector} from 'react-redux';
import {useHistory, useParams} from 'react-router-dom';
import actions from '../../store/actions';
import UiBtnSubmit from '../../components/ui/ui-btn-submit';
import UIClose from '../../components/ui/ui-close';
import FlexBlock from '../../components/flex-block';
import {FieldNotice} from '../../components/field-notice';
import {Component} from '../../components/component';
import {useFormik} from 'formik';
import {validateAddNewParticipant} from '../../utils/validate';
import debounce from '../../utils/debounce';
import UserService from '../../services/UserService';
import InvitationService from '../../services/InvitationService';
import {ToastService} from '../../components/toast/ToastService';
import DfCard from '../../components/df-card';
import {confirm} from '../../components/confirm';
import Headline from '../../components/headline';
import FormAddNewIssue from '../../components/form-add-new-issue';
import {ProgressSpinner} from 'primereact/progressspinner';
import moment from 'moment'
import Notepad from '../../components/notepad';

const DatebookModule = () => {
  const dispatch = useDispatch();
  const params = useParams();
  const history = useHistory();

  const [settingsShow, setSettingsShow] = useState(false);
  const [settingsTarget, setSettingsTarget] = useState('');
  const [settingsAddParticipant, setSettingsAddParticipant] = useState({});
  const [usersIssues, setUsersIssues] = useState(null);
  const [dateDatebook, setDateDatebook] = useState(moment());

  const currentUser = useSelector(state => state.auth.currentUser);
  const datebook = useSelector(state => state.datebook.info);
  const issues = useSelector(state => state.datebook.issues);

  const onChangeDateDatebook = e => {
    setDateDatebook(moment(e.value));
    dispatch(actions.getIssues({idDatebook: datebook.id, date: moment(e.value)}))
  };

  useEffect(() => {
    setUsersIssues(null);

    if (issues) {
      const issuesMap = issues.reduce((res, item) => {
        if (!res[item.target.id]) res[item.target.id] = {user: item.target, issues: []};

        res[item.target.id].issues.push(item);

        return res;
      }, {});

      setUsersIssues(Object.entries(issuesMap).map(([key, obj]) => ({user: obj.user, issues: obj.issues})));
    }
  }, [issues]);

  const formikAddNewParticipant = useFormik({
    initialValues: {
      participant: ''
    },
    validationSchema: validateAddNewParticipant,
    onSubmit: async () => {
      setSettingsAddParticipant({...settingsAddParticipant, isSubmitting: true});
      const invite = {datebook, referral: settingsAddParticipant.user};

      await InvitationService.add(invite)
        .then(res => ToastService.show(res.data))
        .catch(e => ToastService.show(e.response.data, 'error'));

      formikAddNewParticipant.resetForm();
      setSettingsAddParticipant({});
    },
  });

  useEffect( () => {
    dispatch(actions.getDatebook(params.id));
    dispatch(actions.getIssues({idDatebook: params.id, date: moment()}))
  }, []);

  // Поиск пользователя
  const searchParticipant = useCallback(debounce(800, async (val) => {
    setSettingsAddParticipant({notice: 'Ищем...', typeNotice: 'ordinary'});

    let user = await UserService.findOne('email', val);
    user = user.data;

    let participant = user;
    let notice = user ? `${user.username} найден` : 'Пользователь не найден';
    let typeNotice = user ? 'success' : 'error';
    let isSubmitting = null;

    if (user && datebook.participants.find(el => el.id === user.id)) {
      participant = null;
      notice = `${user.username} уже участник этого задачника`;
      typeNotice = 'error';
    }

    setSettingsAddParticipant({user: participant, notice, typeNotice, isSubmitting});
  }), [datebook]);

  const handlerAddParticipant = e => {
    let value = e.target.value;
    let regxEmail = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

    formikAddNewParticipant.setFieldValue('participant', value);

    regxEmail.test(value) ? searchParticipant(value) : setSettingsAddParticipant({})
  };

  const escape = () => {
      dispatch(actions.escapeDatebook(params.id))
        .then(() => history.push('/'))
  };

  return (
    <Styled.DatebookWrap>
      {/*Блок с выбором настроек*/}
      {!settingsTarget && <Styled.DatebookActions>
        {!settingsShow && <Styled.DatebookAction>Настройки &nbsp;
          <button className={'btn btn-tiny'} onClick={() => setSettingsShow(true)}>
            <i className={'fa fa-gears fa-2x icon-primary'}/>
          </button>
        </Styled.DatebookAction>}

        {settingsShow && <>
          {currentUser !== null && currentUser.id == datebook.creator && <>
            <Styled.DatebookActionSmall>
              <span>Добавить участника</span>

              <button className={'btn btn-tiny'} onClick={() => setSettingsTarget('addParticipant')}>
                <i className={'fa fa-user-plus icon-success'}/>
              </button>
            </Styled.DatebookActionSmall>

            <Styled.DatebookActionSmall>
              <span>Удалить участника</span>

              <button className={'btn btn-tiny'} onClick={() => setSettingsTarget('removeParticipant')}>
                <i className={'fa fa-user-times icon-danger'}/>
              </button>
            </Styled.DatebookActionSmall>

            <Styled.DatebookActionSmall>
              <span>Закрыть настройки</span>

              <button className={'btn btn-tiny'} onClick={() => setSettingsShow(false)}>
                <i className={'fa fa-times-circle-o icon-neutral'}/>
              </button>
            </Styled.DatebookActionSmall>
          </>}

          {currentUser !== null && currentUser.id !== datebook.creator && <>
            <Styled.DatebookActionSmall>
              <span>Покинуть ежедневник</span>

              <button className={'btn btn-tiny'} onClick={() => confirm({message: 'Точно покинуть задачник?', accept: escape})}>
                <i className={'fa fa-sign-out icon-danger'}/>
              </button>
            </Styled.DatebookActionSmall>

            <Styled.DatebookActionSmall>
              <span>Закрыть настройки</span>

              <button className={'btn btn-tiny'} onClick={() => setSettingsShow(false)}>
                <i className={'fa fa-times-circle-o icon-neutral'}/>
              </button>
            </Styled.DatebookActionSmall>
          </>}
        </>}
      </Styled.DatebookActions>}

      {/*Настройки*/}
      {settingsTarget && <Component className={'mb-5'}>
        {/*Добавление нового участника*/}
        {settingsTarget === 'addParticipant' && <>
          <Styled.DatebookFormAddParticipant className={'form'} onSubmit={formikAddNewParticipant.handleSubmit}>
            <UIClose cls={'datebook__close-add-participant'}
                     handler={() => {
                       setSettingsTarget('');
                       setSettingsShow(false)
                     }}
            />

            <div className={'form__group'}>
              <h3>Добавить нового участника</h3>

              <FlexBlock wrap={'nowrap'} align={'start'}>
                <FlexBlock flex={'1'} direction={'column'}>
                  <input type={'email'}
                         className={`${(formikAddNewParticipant.errors.participant && formikAddNewParticipant.touched.participant) ? 'error' : ''}`}
                         placeholder={'Введи точный email пользователя'}
                         {...formikAddNewParticipant.getFieldProps('participant')}
                         onChange={handlerAddParticipant}
                  />

                  {formikAddNewParticipant.errors.participant && formikAddNewParticipant.touched.participant && <FieldNotice>{formikAddNewParticipant.errors.participant}</FieldNotice>}
                </FlexBlock>

                &nbsp;&nbsp;&nbsp;

                <UiBtnSubmit isSubmitting={settingsAddParticipant.isSubmitting}
                             disabled={!(formikAddNewParticipant.isValid && formikAddNewParticipant.dirty && settingsAddParticipant.typeNotice === 'success')}
                             cls={'btn btn-tiny'}
                >
                  <i className={'fa fa-plus-square icon-primary'}/>
                </UiBtnSubmit>
              </FlexBlock>
            </div>

            {settingsAddParticipant.notice && <FieldNotice type={settingsAddParticipant.typeNotice}>
              {settingsAddParticipant.notice}
            </FieldNotice>}
          </Styled.DatebookFormAddParticipant>
        </>}

        {/*Удаление участников*/}
        {settingsTarget === 'removeParticipant' && <>
          <UIClose cls={'datebook__close-remove-participant'}
                   handler={() => {
                     setSettingsTarget('');
                     setSettingsShow(false)
                   }}
          />

          <h3>Удаление участников ежедневника '{datebook.title}'</h3>

          {datebook && <>
            {currentUser !== null && <>
              {currentUser.id == datebook.creator && <>
                {datebook.participants.length == 1 && datebook.participants.find(el => el.id == datebook.creator) && <>
                  <p className={'mt-3'}>Ты - единственный участник этого ежедневника</p>
                </>}

                {datebook.participants.length > 1 && <Styled.DatebookRemoveListParticipants className={'mt-3'}>
                  {datebook.participants.map(participant => {
                    if (participant.id !== datebook.creator) {
                      return <DfCard usernameParticipant={participant.username}
                                     avatarParticipant={participant.avatar}
                                     idParticipant={participant.id}
                                     datebookInfo={datebook}
                                     key={participant.id}
                      />
                    }
                  })}
                </Styled.DatebookRemoveListParticipants>}
              </>}
            </>}
          </>}
        </>}
      </Component>}

      {datebook && currentUser && <Styled.DatebookMain className={'mb-5'}>
        <h2>Ежедневник '{datebook.title}'</h2>

        <Headline date={dateDatebook} onChange={onChangeDateDatebook}/>

        {dateDatebook.isSameOrAfter(moment(), 'd') && <Component className={'mb-2 p-2'}>
          <FormAddNewIssue currentUser={currentUser} datebook={datebook} date={dateDatebook}/>
        </Component>}

        {issues && !issues.length && <div className={'flex flex_wrap flex_jc-c--xs mv-8'}>
          <i className={'fa fa-calendar-times-o fa-3x'}/>
          <div className={'w-100 mt-3 tac'}>Задачи отсутствуют</div>
        </div>}

        {issues && !!issues.length && <Styled.DatebookDay>
          <div className={'notepad-list'}>
            {usersIssues && usersIssues.map(notepad => <Notepad notepad={notepad} key={notepad.user.id} />)}
          </div>
        </Styled.DatebookDay>}
      </Styled.DatebookMain>}

      {!datebook && <FlexBlock align={'center'}>
        <ProgressSpinner />
      </FlexBlock>}
    </Styled.DatebookWrap>
  )
};

export default DatebookModule;
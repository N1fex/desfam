import styled from 'styled-components';

export const DatebookWrap = styled.div`
  .datebook__close-add-participant,
  .datebook__close-remove-participant {
    position: absolute;
    top: 15px;
    right: 15px;
  }
`;

export const DatebookMain = styled.div`
  
`;

export const DatebookActions = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 20px;
`;

export const DatebookAction = styled.div`
  display: flex;
  align-items: center;
  font-weight: 600;
  margin-right: 20px;
  
  &:last-child {
    margin-right: 0;
  }
  
  i {
    font-size: 28px;
  }
`;

export const DatebookActionSmall = styled(DatebookAction)`
  font-weight: 400;
  font-size: 14px;

  span {
    margin-right: 10px;
  }

  i {
    font-size: 22px;
  }
  
  @media (max-width: 425px) {
    span {
      display: none;
    }
  }
`;

export const DatebookFormAddParticipant = styled.form`
  i {
    font-size: 35px;
  }
`;

export const DatebookRemoveListParticipants = styled.div`
  display: flex;
  flex-wrap: wrap;
  
  .df-card {
    margin-right: 15px;
  }
`;

export const DatebookDay = styled.div`
  
`;
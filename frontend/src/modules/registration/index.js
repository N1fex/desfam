import React, {useState} from 'react';
import {Component} from '../../components/component';
import {FieldNotice} from '../../components/field-notice';
import {useFormik} from 'formik';
import {validateRegistration} from '../../utils/validate';
import {useDispatch} from 'react-redux';
import actions from '../../store/actions';
import {useHistory} from 'react-router-dom';
import UiBtnSubmit from '../../components/ui/ui-btn-submit';

const RegistrationModule = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [notice, setNotice] = useState({msg: '', type: 'success'});

  const formik = useFormik({
    initialValues: {
      username: '',
      email: '',
      password: '',
      passwordConfirm: ''
    },
    validationSchema: validateRegistration,
    onSubmit: form => {
      setNotice({msg: '', type: 'success'});
      setIsSubmitting(true);

      if (form.passwordConfirm === form.password) {
        dispatch(actions.registration(form))
          .then(() => history.push('/'))
          .catch(e => setNotice({msg: e.response.data, type: 'error'}))
          .finally(() => setIsSubmitting(false))
      } else {
        setNotice({msg: 'Повтор пароля неверный', type: 'error'});
        setIsSubmitting(false)
      }
    },
  });

  return (
    <Component>
      <h1>Регистрация</h1>

      <form className={'form'} onSubmit={formik.handleSubmit}>
        <div className={'form__group'}>
          <label>Назови себя</label>

          <input type={'text'}
                 className={`${(formik.errors.username && formik.touched.username) ? 'error' : ''}`}
                 placeholder={'Желательно ФИО'}
                 {...formik.getFieldProps('username')}
          />

          {(formik.errors.username && formik.touched.username) && <FieldNotice>{formik.errors.username}</FieldNotice>}
        </div>

        <div className={'form__group'}>
          <label>Email</label>

          <input type={'email'}
                 className={`${(formik.errors.email && formik.touched.email) ? 'error' : ''}`}
                 {...formik.getFieldProps('email')}
          />

          {(formik.errors.email && formik.touched.email) && <FieldNotice>{formik.errors.email}</FieldNotice>}
        </div>

        <div className={'form__group'}>
          <label>Пароль</label>

          <input type={'password'}
                 className={`${(formik.errors.password && formik.touched.password) ? 'error' : ''}`}
                 {...formik.getFieldProps('password')}
          />

          {(formik.errors.password && formik.touched.password) && <FieldNotice>{formik.errors.password}</FieldNotice>}
        </div>

        <div className={'form__group'}>
          <label>Повторите пароль</label>

          <input type={'password'}
                 className={`${(formik.errors.passwordConfirm && formik.touched.passwordConfirm) ? 'error' : ''}`}
                 {...formik.getFieldProps('passwordConfirm')}
          />

          {(formik.errors.passwordConfirm && formik.touched.passwordConfirm) && <FieldNotice>{formik.errors.passwordConfirm}</FieldNotice>}
        </div>

        {notice.msg && <FieldNotice type={notice.type}>{notice.msg}</FieldNotice>}

        <div className={'form__actions'}>
          <UiBtnSubmit isSubmitting={isSubmitting}
                       disabled={!(formik.isValid && formik.dirty && !isSubmitting)}
                       cls={'btn btn-primary'}
          >
            Зарегистрироваться
          </UiBtnSubmit>
        </div>
      </form>
    </Component>
  );
};

export default RegistrationModule;
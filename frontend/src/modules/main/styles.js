import styled from 'styled-components';

export const MainWrap = styled.div`
  
`;

export const MainActions = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 20px;
  
  @media (max-width: 425px) {
    padding: 10px;
  }
`;

export const MainAction = styled.div`
  display: flex;
  align-items: center;
  font-weight: 600;
  
  i {
    font-size: 24px;
  }
`;

export const MainCloseAddDatebook = styled.div`
  position: absolute;
  top: 15px;
  right: 15px;
`;
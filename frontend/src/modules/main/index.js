import React, {useEffect, useState} from 'react';
import * as Styled from './styles';
import {Component} from '../../components/component';
import ListDatebooks from '../../components/list-datebooks';
import UIClose from '../../components/ui/ui-close';
import {FieldNotice} from '../../components/field-notice'
import FlexBlock from '../../components/flex-block';
import {useFormik} from 'formik';
import {validateAddNewDatebook} from '../../utils/validate';
import {useDispatch, useSelector} from 'react-redux';
import UiBtnSubmit from '../../components/ui/ui-btn-submit';
import actions from '../../store/actions';
import Invitation from '../../components/invitation';

const MainModule = () => {
  const dispatch = useDispatch();

  const [isSubmittingAddNewDatebook, setIsSubmittingAddNewDatebook] = useState(false);
  const [noticeAddNewDatebook, setNoticeAddNewDatebook] = useState({msg: '', type: 'success'});
  const [showAddNewDatebook, setShowAddNewDatebook] = useState(false);

  const currentUser = useSelector(state => state.auth.currentUser);
  const datebooks = useSelector(state => state.main.datebookList);
  const invitations = useSelector(state => state.main.invitations);

  const formikAddNewDatebook = useFormik({
    initialValues: {
      datebook: ''
    },
    validationSchema: validateAddNewDatebook,
    onSubmit: form => {
      setNoticeAddNewDatebook({msg: '', type: 'success'});
      setIsSubmittingAddNewDatebook(true);

      dispatch(actions.addDatebook(form))
        .then(() => {
          formikAddNewDatebook.resetForm();
          setShowAddNewDatebook(false)
        })
        .catch(e => setNoticeAddNewDatebook({msg: e.response.data, type: 'error'}))
        .finally(() => setIsSubmittingAddNewDatebook(false))
    },
  });

  useEffect(() => {
    if (currentUser) {
      dispatch(actions.getAllDatebooks());
      dispatch(actions.getAllInvitations());
    }
  }, [currentUser]);

  useEffect(() => {
      dispatch(actions.clearDatebook())
  }, []);

  return (
    <Styled.MainWrap>
      {currentUser && <>
        {!!invitations.length && invitations.map(invitation => {
          return <Invitation invitation={invitation} key={invitation.id} />
        })}

        {!showAddNewDatebook && <Styled.MainActions>
          <Styled.MainAction>
            Добавить новый задачник &nbsp;

            <button
              className={'btn btn-tiny'}
              onClick={() => setShowAddNewDatebook(true)}
            >
              <i className={'fa fa-plus-square fa-2x icon-primary'}/>
            </button>
          </Styled.MainAction>
        </Styled.MainActions>}

        {showAddNewDatebook && <Component>
          <Styled.MainCloseAddDatebook>
            <UIClose handler={() => setShowAddNewDatebook(false)}/>
          </Styled.MainCloseAddDatebook>

          <form className={'form'} onSubmit={formikAddNewDatebook.handleSubmit}>
            <div className={'form__group'}>
              <h3>Добавить новый задачник</h3>

              <FlexBlock wrap={'nowrap'} align={'start'}>
                <FlexBlock flex={'1'} direction={'column'}>
                  <input type={'text'}
                         className={`${(formikAddNewDatebook.errors.datebook && formikAddNewDatebook.touched.datebook) ? 'error' : ''}`}
                         placeholder={'Название задачника, сообщества'}
                         {...formikAddNewDatebook.getFieldProps('datebook')}
                  />

                  {formikAddNewDatebook.errors.datebook && formikAddNewDatebook.touched.datebook && <FieldNotice>{formikAddNewDatebook.errors.datebook}</FieldNotice>}
                </FlexBlock>

                &nbsp;&nbsp;&nbsp;

                <UiBtnSubmit isSubmitting={isSubmittingAddNewDatebook}
                             disabled={!(formikAddNewDatebook.isValid && formikAddNewDatebook.dirty && !isSubmittingAddNewDatebook)}
                             cls={'btn btn-tiny'}
                >
                  <i className={'fa fa-plus-square icon-primary'}/>
                </UiBtnSubmit>
              </FlexBlock>
            </div>

            {noticeAddNewDatebook.msg && <FieldNotice type={noticeAddNewDatebook.type}>{noticeAddNewDatebook.msg}</FieldNotice>}
          </form>
        </Component>}

        <Component>
          {!!datebooks.length && <ListDatebooks datebooks={datebooks} />}

          {!datebooks.length && <p style={{textAlign: 'center'}}>Тут появится список ваших задачников</p>}
        </Component>
      </>}

      {!currentUser && <Component>
        <p style={{textAlign: 'center'}}>Войди или зарегистируйся для того, что бы создать задачник</p>
      </Component>}
    </Styled.MainWrap>
  )
};

export default MainModule;
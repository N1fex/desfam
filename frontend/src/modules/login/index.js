import React, {useState} from 'react';
import {Component} from '../../components/component';
import {FieldNotice} from '../../components/field-notice';
import {useFormik} from 'formik';
import {validateLogin} from '../../utils/validate';
import {useDispatch} from 'react-redux';
import actions from '../../store/actions';
import {useHistory} from 'react-router-dom';
import UiBtnSubmit from '../../components/ui/ui-btn-submit';

const LoginModule = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [notice, setNotice] = useState({msg: '', type: 'success'});

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: validateLogin,
    onSubmit: form => {
      setNotice({msg: '', type: 'success'});
      setIsSubmitting(true);

      dispatch(actions.login(form))
        .then(() => history.push('/'))
        .catch(e => setNotice({msg: e.response.data, type: 'error'}))
        .finally(() => setIsSubmitting(false))
    },
  });

  return (
    <Component>
      <h1>Авторизация</h1>

      <form className={'form'} onSubmit={formik.handleSubmit}>
        <div className={'form__group'}>
          <label>Email</label>

          <input type={'email'}
                 className={`${(formik.errors.email && formik.touched.email) ? 'error' : ''}`}
                 {...formik.getFieldProps('email')}
          />

          {(formik.errors.email && formik.touched.email) && <FieldNotice>{formik.errors.email}</FieldNotice>}
        </div>

        <div className={'form__group'}>
          <label>Пароль</label>

          <input type={'password'}
                 className={`${(formik.errors.password && formik.touched.password) ? 'error' : ''}`}
                 {...formik.getFieldProps('password')}
          />

          {(formik.errors.password && formik.touched.password) && <FieldNotice>{formik.errors.password}</FieldNotice>}
        </div>

        {notice.msg && <FieldNotice type={notice.type}>{notice.msg}</FieldNotice>}

        <div className={'form__actions'}>
          <UiBtnSubmit isSubmitting={isSubmitting}
                       disabled={!(formik.isValid && formik.dirty && !isSubmitting)}
                       cls={'btn btn-primary'}
          >
            Вход
          </UiBtnSubmit>
        </div>
      </form>
    </Component>
  )
};

export default LoginModule;
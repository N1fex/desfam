import React, {useEffect} from 'react';
import {BrowserRouter} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import AppWrapper from './components/app-wrapper';
import Navbar from './components/navbar';
import AppRouter from './router';
import './assets/styles/index.scss'
import actions from './store/actions';
import MyToast from './components/toast';
import moment from 'moment';
import 'moment/locale/ru'
import {addLocale} from 'primereact/api';

moment().locale('ru');

addLocale('ru', {
  firstDayOfWeek: 1,
  dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
  monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
});

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    {localStorage.getItem('accessToken') &&
      dispatch(actions.getCurrentUser())
    }
  }, []);

  return (
    <BrowserRouter>
      <AppWrapper>
        <MyToast/>

        <Navbar />

        <AppRouter/>
      </AppWrapper>
    </BrowserRouter>
  );
}

export default App;
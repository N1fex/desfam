import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import MainModule from '../modules/main';
import RegistrationModule from '../modules/registration';
import LoginModule from '../modules/login';
import SettingsModule from '../modules/settings';
import DatebookModule from '../modules/datebook';

const routes = [
  {path: '/', component: MainModule, exact: true},
  {path: '/registration', component: RegistrationModule, exact: true},
  {path: '/login', component: LoginModule, exact: true},
  {path: '/settings', component: SettingsModule, exact: true},
  {path: '/datebooks/:id', component: DatebookModule, exact: true}
];

const AppRouter = () => {
  return (
    <Switch>
      {routes.map(route => <Route
        path={route.path}
        component={route.component}
        exact={route.exact}
        key={route.path}
      />)}

      <Redirect to={'/'}/>
    </Switch>
  )
};

export default AppRouter;